/**
 * This will allow users create their accounts on /admin
 * and will add the following permissions
 */
orion.users.configure({
    forbidClientAccountCreation: true,
    defaultPermissions: [
    	'files.folders',
    	'files.upload',
    	'files.delete',
    	'dictionary.public',
    	'entity.notes.personal', // Users can create, update, and delete notes created by them
		'entity.courses.personal'
      //'pages.public'
    ]
})

if (Meteor.isServer) {
	/**
	 * We will publish the users names
	 */
	Meteor.publish("usersNames", function () {
		return Meteor.users.find({}, { fields: { profile: 1 } });
	});
}