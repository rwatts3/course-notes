/**
 * Declaration of a template
 */
orion.pages.addTemplate({
	/**
	 * The name of the html template
	 */
	template: 'two_column_SL',
	/**
	 * The description of the template.s
	 */
	description: 'Two column template with side column on left.',
	/**
	 * The name display name of the template
	 */
	name: 'Two Column SL'
}, {
	/**
	 * The schema, works like schema on entities
	 */
	icon:{
		type: String,
		label:'Icon'
	},
	content: orion.attribute('froala', {
	    label: 'Content'
	}),
	lc: orion.attribute('froala', {
		label: 'Left Column'
	})
});
