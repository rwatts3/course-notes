/**
 * Declaration of a template
 */
orion.pages.addTemplate({
	/**
	 * The name of the html template
	 */
	template: 'one_column',
	/**
	 * The description of the tempplate.
	 */
	description: 'One column template.',
	/**
	 * The name display name of the template
	 */
	name: 'One Column'
}, {
	/**
	 * The schema, works like schema on entities
	 */
	icon:{
		type: String,
		label:'Icon'
	},
	content: orion.attribute('froala', {
	    label: 'Content'
	})
});
