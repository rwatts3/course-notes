/**
 * Declaration of the notes entity
 * Is a good practice to create a
 * file for each entity.
 */
orion.addEntity('courses', {
    courseID:{
        type: String,
        label: 'Course ID'
    },
    /**
     * The title attribute is saved like a String.
     */
    title: {
        type: String,
        label: "Course Title"
    },

    /**
     * The file attribute is a custom orion attribute
     * This is where orion do the magic. Just set
     * the attribute type and it will automatically
     * create the form for the file.
     * WARNING: the url of the image will not be saved in
     * note.image, it will be saved in note.image.link.
     */
    image: orion.attribute('file', {
        label: 'Course Image',
        optional: true
    }),
    description: {
        type: String,
        label: 'Course Description',
        optional: true,
        autoform: {
            type: 'textarea'
        }
    }
},  {
    icon: 'graduation-cap',
    sidebarName: 'Courses',
    pluralName: 'Courses',
    singularName: 'Course',
    /**
     * You have to put here what do you want to show in
     * the entity index page.
     * It uses aldeed:tabular. Check the documentation
     * https://github.com/aldeed/meteor-tabular/
     */
    tableColumns: [
        { data: 'courseID' , title: 'Course ID'},
        { data:'title', title: 'Title' },
        { data:'description' , title: 'Description'}
    ]
});