/**
 * Declaration of the notes entity
 * Is a good practice to create a
 * file for each entity.
 */
orion.addEntity('notes', {
    /**
     * The title attribute is saved like a String.
     */
    title: {
        type: String,
        label: "Title"
    },

    course: {
        label: "Course",
        type: String,
        optional: false,
        autoform: {
            type: "select",
            options: function () {
                return orion.entities.courses.collection.find().map(function (course) {
                    return {
                        label: course.courseID + ' - ' + course.title,
                        value: course._id
                    };
                });
            }
        }


    },

    /**
     * The file attribute is a custom orion attribute
     * This is where orion do the magic. Just set
     * the attribute type and it will automatically
     * create the form for the file.
     * WARNING: the url of the image will not be saved in
     * note.image, it will be saved in note.image.link.
     */
    image: orion.attribute('file', {
        label: 'Image',
        optional: true
    }),
    /**
     * Here its the same with image attribute.
     * summernote is a html editor.
     * Define froala , or summernote
     */
    body: orion.attribute('froala', {
        label: 'Content',
        optional: true
    })
}, {
    icon: 'bolt',
    sidebarName: 'Notes',
    pluralName: 'Notes',
    singularName: 'Note',
    /**
     * You have to put here what do you want to show in
     * the entity index page.
     * It uses aldeed:tabular. Check the documentation
     * https://github.com/aldeed/meteor-tabular/
     */
    tableColumns: [
        {data: 'title', title: 'Title'},
    /**
     * If you want to show a custom orion attribute in
     * the index table you must call this function
     * orion.attributeColumn(attributeType, key, label)
     */
        orion.attributeColumn('froala', 'body', 'Preview'),
        orion.attributeColumn('file' , 'image' , 'Image')
    ]
});