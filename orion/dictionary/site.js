/**
 * In this file are defined the definitions of the 
 * category basic.
 * It's a good practice to create a file for
 * each dictionary category
 */

/**
 * The siteName definition is special, because 
 * it will change the text of the link to the home 
 * page in the admin.
 */

orion.dictionary.addDefinition('logo', 'site',
    /**
     * The file attribute is a custom orion attribute
     * This is where orion do the magic. Just set
     * the attribute type and it will automatically
     * create the form for the file.
     * WARNING: the url of the image will not be saved in
     * logo, it will be saved in logo.url.
     */
    orion.attribute('file', {
        label: 'Site Logo',
        optional: true
    })
);

orion.dictionary.addDefinition('logoLink', 'site', {
    type: String,
    label: "Logo Link",
    autoform: {
        type: 'url'
    }
});

orion.dictionary.addDefinition('siteName', 'site', {
    type: String,
    label: "Site Name"
});

orion.dictionary.addDefinition('description', 'site', {
    type: String,
    label: "Description of the site",
    autoform: {
        type: 'textarea'
    }
});



