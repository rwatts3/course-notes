Template.homenoteItem.events({
	'click .image': function (event) {
		var note = this;
		var container = $(event.currentTarget);
		container.parent().height(container.outerHeight());
		container.find(".content").animate({ opacity: 0 })
		container.css({
			"z-index": 250,
			position: "fixed",
			top: container.offset().top - $(window).scrollTop() + 10, 
			left: container.offset().left, 
			width: container.width(), 
			height: container.height()
		})
		$(".white-panel").css({ display: 'inherit' })
		$(".white-panel").animate({ opacity: 1 }, function() {
			$("html, body").scrollTop(0);
			container.animate({
				top: 0, 
				left: 0, 
				width: $(window).width()
			}, function() {
				Router.go('note', note);
			})
		})
	}
});

Template.home.rendered = function() {
	$(".notes-list").animate({
		top: 0
	});
  Session.set('homenoteSearch' , '');
}

Template.home.helpers({
  filterednotes: function() {
    var searchFor = Session.get('homenoteSearch');
    if (searchFor === '') {
      return orion.entities.notes.collection.find({});
    } else {
      return orion.entities.notes.collection.find({title: {$regex: searchFor , $options: 'i'}});
    }
  }
});

Template.noteSearch.events({
  'keyup #filter': function(e) {
    Session.set('homenoteSearch' , $('#filter').val());
  }
});

// Startup to set Session for note Search
Meteor.startup(function () {
  Session.set('homenoteSearch' , '');
});